<?php get_header(); ?>

<style type="text/css">
    .card--no-border { border: none; }

.rack iframe {
  border: none;
  min-height: 300px;
  width: 100%;
}

.separator::before {
  left: 25%;
  right: 25%;
}

[ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
  display: none !important;
}

    .masonry {
        -webkit-column-count: 1;
        -webkit-column-gap: .35%;
        -moz-column-count: 1;
        -moz-column-gap: .35%;
        column-count: 1;
        column-gap: .35%;
    }

    @media screen and ( min-width: 768px ) {
        .masonry {
            -webkit-column-count: 3;
            -moz-column-count: 3;
            column-count: 3;
        }
    }

    .masonry__brick {
        display: inline-block;
        position: relative;
        width: 100%;
    }


</style>

<article class="ad ad--hero ad--transparent card clearfix hero--small no-margin" style="background-color: white;">
  <div class="clearfix wrap">
    <div class="col-md--sixcol align-center ad__media">
      <a href="#not-real">
        <img src="http://public.library.nova.edu/wp-content/uploads/2017/01/RAAlogo.png" alt="Read Across America" style="height: 281px;" />
      </a>
    </div>

    <div class="col-md--sixcol">
      <div class="col-md--tencol col--centered ad__copy">
        <header class="card__header">
          <h2>SeussFest!</h2>
            <p class="epsilon no-margin">
                <time class="time"><b>Sunday, March 5th</b> <span class="time__hours" style="color:#999;">12 p.m. - 4 p.m.</span></time>
            </p>
        </header>
        <section class="no-margin">
          <p class="epsilon">A special event celebrating Dr. Seuss’s birthday and the joy of reading. Pre-registration is now closed, but walk-ins will be accepted at the event. See you on Sunday, rain or shine.</p>
        </section>
      </div>
    </div>
  </div>
</article>

<ng-controller data-ng-controller="AdController as adc" data-audience="seussfest" data-ng-cloak>

  <section class="clearfix has-cards hero--small">

    <div class="clearfix masonry wrap" data-ng-cloak>

        <div class="masonry__brick" data-ng-repeat="ad in adc.ads | limitTo: 1" data-ng-cloak>

          <h2 class="align-center delta no-margin hero--small separator separator--top">{{ ad.title }}</h2>

            <a class="link link--undecorated" ng-href="{{ ad.link }}?utm_source=pls&utm_medium=card&utm_campaign=ad-manager">
              <article class="card">
                <div class="card__media">
                  <img ng-src="{{ad.media}}">
                </div>
                <section class="card__content">
                  <p class="zeta">{{ ad.excerpt }}</p>
                </section>
              </article>
            </a>

        </div>

        <div class="masonry__brick">

            <a class="link link--undecorated" href="http://sherman.library.nova.edu/sites/spotlight/events">
                <h2 class="align-center delta no-margin hero--small separator separator--top">More Info</h2>
            </a>

            <a href="http://sherman.library.nova.edu/sites/spotlight/?searchtype=lists&search=Seuss&post_type=list&sortdropdown=r" class="link link--undecorated">
                <div class="card card--no-border no-margin card--shadow" style="background: linear-gradient(to right, white -9%, #61D4E4 55%, white 122%);">
                    <header class="card__header">
                        <svg class="svg" style="margin-right: 1em; float:left; width: 34px; height: 43px;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125">
                            <path d="M94.958 45.535c-.125-.49-.52-.866-1.013-.97l-.605-.126v-8.462l.987-.844c.39-.332.553-.86.42-1.354s-.542-.867-1.046-.958l-5.544-1.006.002-7.124-.002-.147 1.02-.872c.39-.333.554-.86.42-1.354-.134-.496-.54-.87-1.045-.96l-44.816-8.087c-.348-.064-.705.015-.994.217L8.013 37.71c-.656.363-3.058 2.012-3.013 6.595.05 4.977 2.996 7.237 3.122 7.33.017.014.036.022.054.034.03.02.06.04.09.057.034.02.068.035.103.05.03.016.06.03.095.04.038.015.076.025.115.035.022.007.043.016.065.02l2.05.43c-.332.906-.558 2.04-.543 3.465.025 2.466.76 4.264 1.51 5.463l-1.588 1.107c-.66.368-3.058 2.02-3.01 6.596.05 4.977 2.993 7.242 3.122 7.33.11.077.333.194.52.237l47.015 9.865c.006 0 .01.003.016.004l1.68.35c.09.02.18.03.27.03.313 0 .62-.112.86-.318l30.9-26.394c.385-.33.55-.848.424-1.337-.126-.49-.52-.865-1.014-.97l-.552-.114v-7.13l4.23-3.613c.385-.33.55-.85.424-1.338zm-37.542 4.85c.313 0 .62-.11.86-.317l27.245-23.27v8.425L57.17 59.335V50.36c.084.015.165.026.246.026zm-47.35-9.906l44.45 9.326v8.97l-44.45-9.308v-8.99zm2.062 33.606v-8.982l44.45 9.33v8.98l-44.45-9.328zm3.11-13.155v-7.674l40.42 8.48.015.005 1.68.353c.09.02.18.028.27.028.313 0 .62-.11.862-.316l.735-.63.444.094v8.98L15.238 60.93zm72.414-1.14l-28.41 24.185-.008-8.987c.08.016.163.025.245.025.31 0 .617-.11.86-.317l1.498-1.28.663.14c.09.018.183.027.272.027.313 0 .62-.11.86-.317l24.02-20.526v7.05zm3.084-13.16L62.33 70.805l-.01-8.983c.083.015.164.025.245.025.313 0 .62-.11.86-.317L90.74 38.2l-.004 8.43z"/>
                        </svg>
                        <h3 class="card__title delta no-margin" style="position:relative;top:6px;">Book Lists</h3>
                    </header>
                </div>
            </a>

            <a href="http://public.library.nova.edu/about/directions/#parking" class="link link--undecorated">
                <div class="card card--no-border no-margin card--shadow" style="background: linear-gradient(to right, white -9%, #ffae3d 55%, white 122%);">
                    <header class="card__header">
                        <svg class="svg" style="margin-right: 1em; float:left; height: 30px;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 95.502 99.406">
                            <path d="M3.673 75.854c0 2.03 1.64 3.67 3.675 3.67h7.34c2.032 0 3.676-1.64 3.676-3.67v-7.77h58.772v7.77c0 2.03 1.642 3.67 3.672 3.67h7.344c2.033 0 3.676-1.64 3.676-3.67v-7.77H95.5v-9.41H0v9.41h3.673v7.77zM95.502 36.74s-1.385-.46-3.858-1.15L79.834 4.08S55.315 0 47.757 0c-7.562 0-32.082 4.08-32.082 4.08L3.862 35.59C1.384 36.28 0 36.74 0 36.74v18.708h95.502v-18.71zm-81.43 14.618c-3.423 0-6.192-2.77-6.192-6.19 0-3.42 2.77-6.19 6.19-6.19 3.417 0 6.19 2.77 6.19 6.19 0 3.42-2.773 6.19-6.19 6.19zm-1.433-18.303l8.418-22.416c5.622-.905 21.024-3.29 26.697-3.29 5.668 0 21.073 2.384 26.69 3.282l8.405 22.423s-22.273-3.66-35.1-3.66c-12.832 0-35.11 3.66-35.11 3.66zm68.79 18.303c-3.415 0-6.186-2.77-6.186-6.19 0-3.42 2.772-6.19 6.187-6.19 3.423 0 6.19 2.77 6.19 6.19.002 3.42-2.767 6.19-6.19 6.19z"/>
                        </svg>
                        <h3 class="card__title delta no-margin" style="position:relative;top:6px;">Parking</h3>
                    </header>
                </div>
            </a>

            <a class="link link--undecorated" data-ng-init="scheduleModal=0" data-ng-click="scheduleModal=1">
                <div class="card card--no-border no-margin card--shadow" style="background: linear-gradient(to right, white -9%, #e2624f 55%, white 122%);">
                    <header class="card__header">
                        <svg class="svg" style="float: left; margin-right: 1em;" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path d="M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8z"></path>
                            <path d="M0 0h24v24H0z" fill="none"></path>
                            <path d="M12.5 7H11v6l5.25 3.15.75-1.23-4.5-2.67z"></path>
                        </svg>
                        <h3 class="card__title delta no-margin" style="position:relative;top:6px;">Schedule</h3>
                    </header>
                </div>
            </a>

          <section class="modal semantic-content"  data-ng-class="{ 'is-active' : scheduleModal === 1 }" tabindex="-1" role="dialog" aria-labelledby="modal-label" aria-hidden="true">
            <div class="modal-inner">
              <div id="modal-label"><h3>Schedule</h3></div>
              <div class="modal-content clearfix" style="background-color: white; color: #444;">
                <h4>LIBRARY LAWN-MAIN STAGE</h4>
                <ul>
                  <li>12 - 12:30 PM Cat In The Hat Opens Event with Miss Penny of South Florida WPBS!</li>
                  <li>12:30- 1 PM  Kids Gotta Rock! Live Concert with Mr.Richard</li>
                  <li>1 - 2 PM Storytelling with Miss Gail</li>
                  <li>2 - 3 PM Crazy Karaoke with Fantasy Theatre Factory</li>
                  <li>3 - 4 PM  It’s Magic with James Changefield</li>
                </ul>

                <h4>ALVIN SHERMAN LIBRARY</h4>
                <ul>
                  <li>2nd Floor, Cotilla Gallery</li>
                  <li>1 - 1:30 PM “Hat’s Off to Reading” with Miss Penny and Special Guest</li>
                  <li>2 - 2:30 PM  “Hat’s Off to Reading” with Miss Penny and Special Guest</li>
                  <li>3- 3:30 PM“Hat’s Off to Reading” with Miss Penny and Special Guest</li>
                </ul>

                <h4>MINIACI THEATER</h4>
                <ul>
                  <li>12-12:30 PM SEATING IS ‘FIRST COME, FIRST SERVED</li>
                  <li>12:30 - 1:30 PM Bucky and Gigi Show</li>
                  <li>2 - 2:45 PM Kids Gotta Rock! Live Concert With Mr. Richard</li>
                  <li>3 - 4 PM Bucky and Gigi Show</li>
                </ul>
              </div>
            </div>
            <a href class="modal-close" title="Close this modal" data-close="Close" data-dismiss="modal" ng-click="scheduleModal = 0">×</a>
          </section>

            <!--<a href="" class="link link--undecorated">
                <div class="card card--no-border no-margin card--shadow" style="background: linear-gradient(to right, white -9%, #f3787a 55%, white 122%);">
                    <header class="card__header">
                        <svg xmlns="http://www.w3.org/2000/svg" class="svg" viewBox="0 0 100 125" style="margin-right: 1em; float:left; width: 34px; height: 42px;"><switch><foreignObject width="1" height="1" x="0" y="0" requiredExtensions="http://ns.adobe.com/AdobeIllustrator/10.0/"/><g><path d="M41.26 44.097c-.504.506-.352.822-.265.996.303.597.63 1.344.833 2.058.08.286.225.77.73.267l4.65-4.65L58.122 31.85c1.31-1.31 2.93-2.107 4.56-2.25 1.315-.113 2.588.207 3.695.907l.125-.124c.902-.902 1.47-1.932 1.603-2.898.146-1.068-.356-2.16-1.284-3.088-.845-.845-2.066-1.397-3.4-1.062-1.2.304-2.51 1.08-3.51 2.077l-.8.802c-.135.154-.27.308-.42.456L44.973 40.385l-3.712 3.712z"/><path d="M20.62 77.23l1.322-1.324s.554-.55.554-1.6V64.23c0-.445-.002-.868-.005-1.272-.024-4.45-.04-7.138 3.838-11.018l7.71-7.71.037-.036 2.15-2.15c.532-.53 1.365-.607 1.984-.18.023.015.14.097.32.24.26.264.705.332 1.172-.137l3.36-3.357-.02-.018L57.11 24.525c1.112-1.338 1.538-3.182.105-4.615-1.57-1.568-3.635-.91-4.985.44L31.922 40.657l-1.324 1.323-.69.69-1.316 1.316-1.005 1.006c-.608.607-1.592.607-2.2 0-.607-.608-.607-1.593 0-2.2L29.58 38.6c1.08-1.14 1.37-2.592 1.26-3.816-.102-1.116-.633-2.062-1.18-2.76-.05-.063-.38-.428-.743-.065l-8.485 8.48c-2.96 2.96-2.95 4.55-2.924 8.8.003.41.006.838.006 1.29V62.3c0 .412-.164.81-.456 1.1l-6.552 6.554c-.797.797-.17 1.23-.17 1.23l8.63 6.08c-.002 0 .855.762 1.653-.035z"/><path d="M78.687 58.827c-.738.142-1.568.642-2.407 1.444l-8.45 8.45-3.38 3.38-1.398 1.4c-.304.304-.7.456-1.1.456-.397 0-.795-.152-1.1-.456-.607-.608-.607-1.59 0-2.2l.216-.216.696-.695 4.654-4.656 7.645-7.645 4.75-4.753c1.35-1.35 2.01-3.415.44-4.985-1.393-1.394-3.175-1.028-4.502.015-.104.084-.21.173-.31.265-.06.054-.115.105-.17.162l-12.71 12.71-.134.133-.53.53-.127.126-.89.89-2.25 2.25c-.075.073-.154.136-.236.19-.353.238-.78.315-1.185.23-.286-.06-.56-.197-.78-.42-.61-.607-.61-1.592 0-2.2l1.942-1.943 1.366-1.366.012-.014 1.468-1.468 11.85-11.847c.044-.045.09-.084.137-.128l1.59-1.59.798-.798c.902-.905 1.472-1.935 1.604-2.9.143-1.07-.237-2.04-1.165-2.968-.846-.846-1.905-1.092-3.236-.756-1.2.304-2.515 1.08-3.51 2.077l-.804.802-.4.4-13.75 13.755-4.755 4.79c-.303.297-.695.446-1.09.446-.396 0-.795-.15-1.1-.455-.32-.32-.467-.746-.448-1.17.015-.372.16-.74.45-1.03l3.063-3.063 1.517-1.517c.05-.068.1-.137.16-.197L65.2 38.225c.746-.898 1.182-2.022.955-3.1-.104-.496-.352-.98-.77-1.428-.028-.03-.05-.06-.08-.09-.392-.39-.815-.644-1.25-.78-1.307-.415-2.722.21-3.735 1.222L49.408 44.96l-5.503 5.504-3.89 3.89-1.324 1.322-.686.69-1.317 1.317-1.005 1.005c-.607.607-1.59.607-2.2 0-.606-.607-.606-1.592 0-2.2l4.193-4.19c1.1-1.126 1.37-2.594 1.26-3.817-.047-.512-.185-.99-.373-1.424-.208-.475-.475-.897-.75-1.258-.144-.19-.465-.48-1.053.11-.59.587-8.235 8.23-8.235 8.23-2.96 2.962-2.95 4.55-2.924 8.8 0 .408.004.837.004 1.29v11.768c0 .41-.163.808-.455 1.1l-2.66 2.654-1.115 1.116-1.117 1.115-1.62 1.62c-.878.88-.022 1.41-.022 1.41l18.18 12.815s.99.688 1.728-.05l4.092-4.092c.326-.326.784-.49 1.24-.45.057.006 6.26.5 9.777-3.015l15.102-15.103.002.012.18-.18.24-.242L79.79 64.082c1.8-1.8 2.096-3.33.88-4.546-.818-.816-1.523-.795-1.98-.71zM42.965 15.795c.304.304.702.455 1.1.455s.796-.15 1.1-.455c.606-.607.606-1.592 0-2.2L41.017 9.45c-.607-.608-1.592-.608-2.2 0s-.606 1.59 0 2.198l4.147 4.147zM34.1 24.66c.304.304.702.455 1.1.455s.796-.15 1.1-.455c.608-.607.608-1.592 0-2.2l-4.147-4.146c-.607-.607-1.592-.607-2.2 0s-.606 1.592 0 2.2L34.1 24.66zm33.457-8.41c.398 0 .796-.15 1.1-.455l4.268-4.27c.607-.606.607-1.59 0-2.198s-1.592-.607-2.2 0l-4.267 4.27c-.607.606-.607 1.59 0 2.198.303.304.7.455 1.1.455zm8.01 10.654c.396 0 .795-.152 1.1-.456l4.267-4.268c.607-.607.607-1.592 0-2.2s-1.592-.606-2.2 0l-4.267 4.27c-.607.606-.607 1.59 0 2.198.304.304.7.456 1.1.456zm11.977 3.264c-.607-.607-1.592-.607-2.2 0l-4.268 4.27c-.607.606-.607 1.59 0 2.198.305.304.702.455 1.1.455.398 0 .796-.15 1.1-.452l4.27-4.27c.605-.606.605-1.59-.002-2.198zM56.29 11.393c.857 0 1.554-.696 1.554-1.556v-6.28c0-.86-.697-1.557-1.555-1.557-.86 0-1.56.696-1.56 1.556v6.28c0 .86.698 1.557 1.557 1.557z"/></g></switch></svg>
                        <h3 class="card__title delta no-margin" style="position:relative;top:6px;">Sponsors</h3>
                    </header>
                </div>
            </a>-->

        </div>

        <ng-repeat data-ng-repeat="ad in adc.ads | limitTo: 5" data-ng-cloak>
            <div class="masonry__brick" data-ng-if="$index > 0">
                <h2 class="align-center delta no-margin hero--small separator separator--top">{{ ad.title }}</h2>

                <a class="link link--undecorated" ng-href="{{ ad.link }}?utm_source=pls&utm_medium=card&utm_campaign=ad-manager">
                    <article class="card">
                        <div class="card__media">
                            <img ng-src="{{ad.media}}">
                        </div>
                        <section class="card__content">
                            <p class="zeta">{{ ad.excerpt }}</p>
                        </section>
                    </article>
                </a>
            </div>

        </ng-repeat>

      </div><!--/.wrap-->
  </section>
</ng-controller>


<?php get_footer(); ?>

